#include <TaskGroup.hpp>


void TaskGroup::add_to_task_list(Task &task)
{
	task_list.push_back(&task);
}


//Inits all task list.
void TaskGroup::init_task_list()
{
	for (auto & task : task_list)
	{
		task->init();
	}
}


void TaskGroup::finalize_task_list()
{
	for (auto & task : task_list)
	{
		task->finalize();
	}
}