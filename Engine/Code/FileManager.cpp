#include "FileManager.hpp"

	FileManager::FileManager(string & file_path, shared_ptr<Kernel> kernel_) : kernel(kernel_)
	{
		cout << "Serializing..." << endl;
		//Check Kernel instance is not null.
		parseXml(file_path);
	}


	FileManager::~FileManager()
	{
	}


	void FileManager::parseXml(string & file_path)
	{
		fstream xml_file(file_path, fstream::in);
		if (xml_file.good())
		{
			cout << "Good file. Continuing..." << endl;
			vector <char> xml_content;

			bool finished = false;

			do
			{
				int character = xml_file.get();

				if (character != -1)
				{
					xml_content.push_back((char)character);
				}
				else
					finished = true;
			} while (!finished);


			xml_content.push_back(0);

			//Parse xml loaded from vector

			rapidxml::xml_document < > document;

			document.parse<0>(xml_content.data());

			//Get pointer to the first xml node:
			rapidxml::xml_node <>* node = document.first_node();

			if (node == nullptr)
			{
				cout << "Nullptr on root node" << endl;
				system("pause");
				exit(-1);
			}

			else if (node == NULL)
			{
				cout << "NULL (0) on root node. Aborting..." << endl;
				system("pause");
				exit(-1);
			}

			else
			{
				cout << "" << endl;
				cout << "" << endl;
				cout << "Done parsing nodes!" << endl;
				cout << "" << endl;
				cout << "" << endl;
				printXml(node, false);
				cout << "" << endl;
				cout << "" << endl;
				cout << "" << endl;
				cout << "" << endl;
				cout << "" << endl;
			}
		}


		else
		{
			cout << "Error. Exiting..." << endl;
			system("pause");
			exit(-1);
		}

	}

	//Variadic templates.
	void FileManager::printXml(const rapidxml::xml_node<>*node, ...)
	{
		cout << string(this->depth_counter * 3, ' ').c_str() << node->name() << endl;

		if (node->name() == "Entity")
			cout << "Node is an entity." << endl;
		checkNode(node->name());
		// Add event to a queue to check what kind of event is it.
		// Se muestran todos los hijos del nodo:

		for (xml_node<> * child = node->first_node(); child; child = child->next_sibling())
		{
			if (child->type() == node_element)
			{
				printXml(child, this->depth_counter += 1);
			}
		}
	}


	void FileManager::checkNode(const string &node_name)
	{
		//Si crea una entidad, habr� que llamar a los metodos de �sta, 
		//porque seguramente tenga componentes.
		if (node_name == "entity")
		{
			cout << "Posible entity task here." << endl; 
			//Now we have to check it's child nodes. 
		}
	}


