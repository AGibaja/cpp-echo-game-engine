#include "Task.hpp"


Task::Task()
{
	cout << "Default constructor called." << endl; 
	this->id = "Non assigned id.";
}


Task::Task(int priority_, bool finished_)
{
	cout << "Param constructor called at task." << endl;
	this->id = "Non assigned id."; 
}


shared_ptr<Task> Task::getTask()
{
	if (this!= nullptr)
		return shared_ptr<Task>(this);
	else
	{
		cout << "Doesn't point to nothing. Nullptr." << endl; 
		return nullptr; 
	}
}


void Task::setTaskId(string id_)
{
	this->id = id_; 
}


shared_ptr<string> Task::getTaskId()
{
	shared_ptr <string> id_pointer = make_shared <string> (this->id);
	return id_pointer; 
}
