#include <TaskComponent.hpp>
#include <iostream>
#include <Scene.hpp>


using namespace std; 


TaskComponent::TaskComponent(int priority_, bool finished_, shared_ptr<Entity> entity, string comp_id) :
	Task(priority_,finished_), entity_owner(entity), component_id(comp_id)
{
	init();
}


TaskComponent::TaskComponent(shared_ptr<Scene> scene_, int priority_, bool finished_, string entity_id, string component_id) :
	Task(priority_, finished_), component_id(component_id), scene_kernel(scene_)
{
	bool entity_found = false; 
	if (scene_kernel == nullptr)
		cout << "Nullptr scene kernel." << endl; 
	else if (scene_kernel != nullptr)
	{
		cout << "Success!" << endl; 
	}
	for (auto &e : scene_kernel->entities)
	{
		if (entity_id == e.second->entity_id)
		{
			cout << "Entity with id: " << entity_id << "found." << endl;
			shared_ptr<Component> comp = make_shared <ControlComponent>(this->entity_owner, this->component_id);
			e.second->addComponent(comp); 
			//e.second->printComponents();
			entity_found = true;
		}

	}

	if (!entity_found)
		cout << "Entity not found." << endl; 
}


void TaskComponent::init()
{
	//TODO
	cout << "Component task init" << endl; 

}


void TaskComponent::run()
{

	cout << "Running started of task" << endl; 
}


void TaskComponent::finalize()
{
	this->finished = true; 
	cout << "Ending Task" << endl; 
}
