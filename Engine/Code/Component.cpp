#include "Component.hpp"



Component::Component()
{

}


Component::Component(shared_ptr<Entity>entity) : parent(entity), component_id("Unnamed Component")
{

}


Component::Component(shared_ptr<Entity>entity, string id_component) : parent(entity), component_id(id_component)
{

}


Component::~Component()
{

}
