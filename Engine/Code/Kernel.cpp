#include "Kernel.hpp"
#include <iostream>
#include <sstream>
#include <TaskEntity.hpp>
#include <./SDL.h>
#include <TaskListener.hpp>
#include <TaskComponent.hpp>


using namespace std;


Kernel::Kernel()
{
	std::cout << "Kernel Constructor reached" << std::endl;
	task_list = vector<unique_ptr<Task>>();
	running = true; 
}


void Kernel::init()
{
	for (auto & task : task_list)
	{
		task->init();
	}
	
}


void Kernel::execute()
{
	
	for (auto &t : task_list)
	{
		if (t->finished)
			cout << "Not finished task: " << t->getTaskId() << endl; 
	}

	for (;;)
	{
		SDL_Event event;

		/* Poll for events. SDL_PollEvent() returns 0 when there are no  */
		/* more events on the event queue, our while loop will exit when */
		/* that occurs.                                                  */
		while (SDL_PollEvent(&event) && running)
		{
			/* We are only worried about SDL_KEYDOWN and SDL_KEYUP events */
			switch (event.type)
			{
			case SDL_KEYDOWN:
			{
				if (this->scene_kernel == nullptr)
					cout << "nullptr scene" << endl; 
				
				unique_ptr<Task> new_task = make_unique<TaskEntity>(5, false, this->scene_kernel);

				
				this->addTask(move(new_task));

				//If A is pressed.
				if (event.key.keysym.scancode == 4)
				{
					cout << "A pressed" << endl;
					
					unique_ptr<Task> task_listener = make_unique<TaskListener>(5, false, "Entity Movable", this->scene_kernel);
					unique_ptr<Task> task_component = make_unique<TaskComponent>(this->scene_kernel, 5, false, "Entity Movable", "Movable component");
				}

				//B pressed.
				else if (event.key.keysym.scancode == 5)
				{
					cout << "B pressed." << endl; 
					//for (auto &m : scene_kernel->entities)
				}

				break;
			}
				
			case SDL_MOUSEBUTTONDOWN:
			{
				this->checkEntities();
				break;
			}
				
			case SDL_QUIT:
			{
				//Unhandled exit, caution. 
				cout << "Quitting... " << endl; 
				exit(0); 
				running = false; 
			}

			default:
				break;
			}
			if (!this->task_list.empty())
			{
				cout << "Some tasks are here. Finish them." << endl;
				this->runTasks(); 
				this->checkFinalizeTasks(); 
			}
		}
	}
}


void Kernel::addTask(unique_ptr<Task> task)
{
	
	std::stringstream ss;
	ss << "\t" << task_list.capacity();
	std::string capacity_now = ss.str();

	cout << "Task added" << capacity_now << "added." << endl;

	task->setTaskId(capacity_now);
	task_list.push_back(move(task));
}


void Kernel::runTasks()
{
	for (auto & task : task_list)
	{
		task->run();
	}
}


void Kernel::checkFinalizeTasks()
{
	for (auto &task : task_list)
	{
		if (task->finished)
			removeLastTask(); 
	}
}


void Kernel::checkEntities()
{
	cout << "Check entities called." << endl; 
	if (this->scene_kernel == nullptr)
	{
		cout << "Nullptr scene_kernel" << endl; 
		cout << "Nullptr scene_kernel" << endl;
		cout << "Nullptr scene_kernel" << endl;
		cout << "Nullptr scene_kernel" << endl; 
	}
	if (this->scene_kernel->entities.empty())
	{
		cout << "Empty entity list." << endl; 
		cout << "Empty entity list." << endl;
		cout << "Empty entity list." << endl;
		cout << "Empty entity list." << endl;
	}
	for (auto &entity : this->scene_kernel->entities)
	{
		cout << "Entities: " << entity.first << endl; 
		entity.second->printComponents(); 
	}
}


void Kernel::setSceneKernel(shared_ptr<Scene> scene)
{
	this->scene_kernel = scene; 
	cout << "Scene name is: " << this->scene_kernel->id_scene << endl; 
}


void Kernel::removeLastTask()
{
	this->task_list.pop_back(); 
	cout << "Removed last task." << endl; 
}


shared_ptr<Scene> Kernel::getSceneKernel()
{
	if (this->scene_kernel != nullptr)
		return this->scene_kernel;

	else if (this->scene_kernel == nullptr)
	{
		cout << "Scene kernel from kernel is nullptr" << endl; 
		return nullptr;
	}

	else
	{
		cout << "Unknown error in scene kernel." << endl;
		return nullptr;
	}
}