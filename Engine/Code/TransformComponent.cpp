#include <TransformComponent.hpp>


TransformComponent::TransformComponent() : id_component("Unnamed transform component")
{
	initialize(); 
}


TransformComponent::TransformComponent(shared_ptr<Entity> entity) : entity_parent(entity)
{
	initialize();
}


TransformComponent::TransformComponent(string name) : id_component(name)
{
	initialize(); 
}


TransformComponent::TransformComponent(shared_ptr<Entity>entity, string id_component_) : entity_parent(entity), id_component(id_component_)
{
	initialize();
}


void TransformComponent::initialize()
{
	this->position = { 0,0,0 };
}


string &TransformComponent::getId()
{
	return id_component; 
}


void TransformComponent::update(float deltatime)
{

}


void TransformComponent::translate(glm::vec3 vec)
{
	this->position = (this->position + vec); 
}

