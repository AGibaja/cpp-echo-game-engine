#include <Entity.hpp>


Entity::Entity(shared_ptr<Scene> scene) : parent_scene(scene), entity_id("Unnamed entity")
{
	this->components = map<string, shared_ptr<Component>>(); 
}


Entity::Entity(shared_ptr<Scene> scene, string name) : parent_scene(scene), entity_id(name)
{
	this->components = map<string, shared_ptr<Component>>();

}


shared_ptr<Component> Entity::getComponent(string id_component)
{
	for (auto &c : components)
	{
		if (c.second->getId() == id_component)
		{
			cout << "Component with name: " << id_component << " found." << endl; 
			return c.second; 
		}

		else
		{
			cout << "Component with id: " << id_component << " not found. Returning nullptr." << endl;
			return nullptr; 
		}
	}
}


void Entity::initialize()
{
	cout << "Entity with name: " << this->entity_id << " initialized" << endl; 
	if (components.empty())
		cout << "No components in this entity." << endl; 
	else
	{
		for (auto & component : components)
		{
			component.second->initialize();
		}
	}
	
}


void Entity::update(float deltaTime)
{
	for (auto & component : components)
	{
		component.second->update(deltaTime);
	}
}


void Entity::addComponent(const string &name, shared_ptr<Component> &component)
{
	cout << "" << endl;
	cout << "" << endl;
	cout << "Component added" << endl; 
	cout << "" << endl;
	cout << "" << endl;
	components[name] = component;
}


void Entity::addComponent(shared_ptr<Component> &component)
{
	cout << "" << endl;
	cout << "" << endl;
	cout << "Component added" << endl;
	cout << "" << endl;
	cout << "" << endl;
	if (component == nullptr)
		cout << "Component is nullptr." << endl; 
	else
	{
		cout << "Component not nullptr." << endl; 
		components["Unnamed Component"] = component; 
	}
		
}



void Entity::printComponents()
{
	if (components.empty())
	{
		cout << "No components in this entity." << endl; 
	}
	else
	{
		for (auto &c : components)
		{
			cout << "Name of component is: " << c.second->getId() << endl;
		}
	}
	
}


void Entity::setEntityId(string name)
{
	this->entity_id = name; 
}


Entity::~Entity()
{

}