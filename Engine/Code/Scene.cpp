#include <Scene.hpp>
#include <iostream>
#include <FileManager.hpp>
#include <TaskEntity.hpp>


Scene::Scene(std::string & file_path, shared_ptr<Kernel> kernel_) : kernel(kernel_)
{
	cout << "Starting Scene..." << endl; 
	this->load(file_path); 
	this->entities = map <string, shared_ptr<Entity>>();
}


Scene::~Scene()
{

}


//We should parse an xml and intialize it as we want to there. For debugging purposes we'll
//just hardcode it. 
void Scene::load(std::string & file_path)
{
	FileManager *fm = new FileManager(file_path, kernel);
	cout << "XML parsing done!. Execute the kernel." << endl;
	this->id_scene = "Scene1"; 
}


void Scene::execute()
{
	kernel->setSceneKernel(shared_ptr<Scene>(this));
	kernel->execute(); 
}


void Scene::addEntity(const string &name, shared_ptr<Entity> &entity_)
{
	cout << "" << endl;
	cout << "" << endl;
	cout << "Entity added" << endl;
	cout << "" << endl;
	cout << "" << endl;
	entities[name] = entity_;
}