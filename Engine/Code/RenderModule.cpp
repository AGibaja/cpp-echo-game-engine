#include <RenderModule.hpp>


RenderModule::RenderModule(Scene *parent_scene)
{
	cout << "Receiving render module." << endl;
}


RenderModule::TaskRender::TaskRender(RenderModule &module) : Task(5, true), render_module(module)
{
	this->finished = false;
}


void RenderModule::TaskRender::init()
{
	std::cout << "Task Render Implementation init()!" << std::endl;
}


void RenderModule::TaskRender::run()
{
	//render_module.render(); 
	render_module.scene->window->getWindow()->swapBuffers();

}


void  RenderModule::TaskRender::finalize()
{
	std::cout << " Task Render Implementation finalize()!" << std::endl;
}