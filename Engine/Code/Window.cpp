
// COMENTARIO DE CABECERA

#include <SDL.h>
#include <Window.hpp>
#include <iostream>
#include <ciso646>


namespace engine
{

	Window::Window(const char * title, int width, int height)
	{
		if (!SDL_WasInit(SDL_INIT_VIDEO))
		{
			SDL_Init(SDL_INIT_VIDEO);
		}
		
		this->sdl_window = SDL_CreateWindow(
			"SDL2/OpenGL Demo", 100,100, 640, 480,
			SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

		if (not sdl_window)
		{
			std::cout << "No se ha podido crear una ventana." << std::endl;
		}
		else
		{
			std::cout << "Ventana creada" << std::endl;
			//Inicializamos el contexto de GLEW 
			gl_context = SDL_GL_CreateContext(this->sdl_window);
			glewInit(); 
			this->makeCurrent();
		}
	}

	///Otros m�todos �tiles para ventana (fullscreen y cosas as�).
	
	void Window::makeCurrent()
	{
		SDL_GL_MakeCurrent(this->sdl_window, this->gl_context);
	}

	void Window::swapBuffers()
	{
		SDL_GL_SwapWindow(this->sdl_window);
	}

	SDL_Window* Window::getSdlWindow()
	{
		return this->sdl_window; 
	}

	Window *Window::getWindow()
	{
		return this;
	}

	Window::~Window()
	{
		//SDL_DestroyWindow(this->sdl_window);
	}
}