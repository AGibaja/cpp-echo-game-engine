#include "ComponentRender.hpp"



ComponentRender::ComponentRender(shared_ptr<Entity>entity) : Component(Component::parent, Component::component_id)
{

}


ComponentRender::ComponentRender(shared_ptr<Entity>entity, string id) : Component(Component::parent, Component::component_id)
{

}


void ComponentRender::update(float delta_time)
{
	//Transform matrix of Entity
	//and assign to model.
	std::cout << "Component Render Implementation update()!" << std::endl;

}


void ComponentRender::initialize()
{
	std::cout << "Task Render Implementation initialize()!" << std::endl;
}


Task * ComponentRender::get_task()
{
	return task;
}


shared_ptr <Component> ComponentRender::createComponent()
{
	shared_ptr <Component> component(this);
	return component;
}


void ComponentRender::render()
{
	std::cout << "Rendering!" << std::endl;
}


void ComponentRender::swapBuffersWindow()
{
	this->window->swapBuffers();
}
