#include <TaskEntity.hpp>
#include <ControlComponent.hpp>



TaskEntity::TaskEntity(int priority_, bool finished_, shared_ptr<Scene> scene_) :
	Task(priority_, finished_), scene(scene_)
{
	init();
}

TaskEntity::TaskEntity(int priority_, bool finished_) :
	Task(priority_, finished_)
{
	init();
}


TaskEntity::TaskEntity(int priority_, bool finished_, shared_ptr<Scene> scene_, shared_ptr <Component> component) : 
	Task(priority_, finished_), scene(scene_)
{
	init(); 
}


TaskEntity::TaskEntity(int priority_, bool finished_, vector<shared_ptr <Component>> components_list)
{
	init();
}


void TaskEntity::init()
{
	shared_ptr<Entity>new_entity = make_shared<Entity>(scene, "Entity Movable");
	this->entity_pointer = new_entity;
	shared_ptr<Component> transform = make_shared<TransformComponent>(new_entity, "Component Transform");

	new_entity->initialize();
	new_entity->addComponent(transform->component_id, transform);
	this->scene->addEntity(new_entity->entity_id, new_entity);
}



void TaskEntity::finalize()
{
	this->finished = true; 
	std::cout << "finalizing task" << std::endl;
}


void TaskEntity::run()
{
	std::cout << "Running task" << std::endl;	
	this->entity_pointer->printComponents(); 
	finalize(); 
}


TaskEntity::~TaskEntity()
{

}