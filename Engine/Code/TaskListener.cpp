#include <TaskListener.hpp>



TaskListener::TaskListener() 
{
	init();
}


TaskListener::TaskListener(int priority_, bool finished_) : Task(Task::priority, Task::finished)
{
	cout << "Warning, no entity attached to listener about to be created." << endl; 
	init(); 
}


TaskListener::TaskListener(int priority_, bool finished, shared_ptr<Entity> entity_pointer) :
	Task(Task::priority, Task::finished), entity_owner(entity_pointer)
{
	
	init();
}


TaskListener::TaskListener(int priority_, bool finished, string entity_id, shared_ptr<Scene> scene) :
	Task(Task::priority, Task::finished), scene_entity(scene)
{
	if (scene_entity == nullptr)
	{
		cout << "Scene entity parameter is nullptr" << endl; 
	}
	

	else
	{
		bool entity_found = false; 
		for (auto &e : this->scene_entity->entities)
		{
			if (e.first == entity_id)
			{
				for (int i = 0; i <= 10; i++)
					cout << "Entity we were looking for with id: " << entity_id << " has been found." << endl;
				entity_found = true;
				//Now print all it's components.
				e.second->printComponents(); 
				e.second->getComponent("Component Transform"); 
				cout << "FOUND COMPONENT" << endl; 
			}
		}
		
	}
}


void TaskListener::init()
{

	if (this->entity_owner->getComponent("Component Transform") != nullptr)
	{
		for (int i = 0; i <= 40; i++)
		{
			cout << "Component transform founded" << endl; 
		}
	}

	else if (this->entity_owner->getComponent("Component Transform") == nullptr)
	{
		for (int i = 0; i <= 40; i++)
		{
			cout << "Nullptr or component not found." << endl;
		}
	}

}


void TaskListener::run()
{

}


void TaskListener::finalize()
{

}


void TaskListener::setSceneListener(shared_ptr<Scene> scene)
{
	this->scene_entity = scene; 
}

