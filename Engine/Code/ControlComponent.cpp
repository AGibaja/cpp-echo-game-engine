#include <ControlComponent.hpp>


ControlComponent::ControlComponent(shared_ptr <Entity> e)
{
	initialize(); 
}


ControlComponent::ControlComponent(shared_ptr <Entity> e, string id) : control_component_id(Component::component_id)
{
	initialize(); 
}


void ControlComponent::initialize()
{
	ListenerMovement *lm = new ListenerMovement(this->parent);
	this->addListener("Movement.", lm);
	cout << "Initializing control component." << endl; 
}


void ControlComponent::update(float delta_time)
{
	cout << "Updating control component." << endl; 
}


void ControlComponent::handle(const Message & message)
{
	cout << "Message received is: " << message.id << endl; 
}


string& ControlComponent::getId()
{
	return this->control_component_id; 
}