#include <Dispatcher.hpp>


//Dispatcher es indepndiente del receptor y el emisor. Es el medio.

void Dispatcher::addListener(string message_id, Listener * l)
{
	listeners[message_id].push_back(l); 
}


void Dispatcher::send(Message &m)
{
	list< Listener * > & list = listeners[m.id];

	for (auto *listener : list)
	{
		listener->handle(m);
	}
}
