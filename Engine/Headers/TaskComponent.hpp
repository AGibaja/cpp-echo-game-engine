#include <Kernel.hpp>
#include <Task.hpp>
#include <ControlComponent.hpp>


//Class Task_Entity used to create new Components.
class TaskComponent : public Task
{
public: 
	
	TaskComponent(int priority, bool finished, shared_ptr<Entity> entity, string component_id);
	TaskComponent(shared_ptr<Scene> scene_, int priority, bool finished, string entity_id, string component_id);

	string component_id;
	shared_ptr <Entity> entity_owner; 
	shared_ptr <Scene> scene_kernel; 

	void init();
	void run();
	void finalize();
};
