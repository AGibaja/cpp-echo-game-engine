#include <Task.hpp>
#include <Scene.hpp>


//Class Task_Entity used to create new Entities
class TaskEntity : public Task
{

public:

	shared_ptr<Scene> scene;
	shared_ptr<Entity> entity_pointer;

	bool finished;

	//Two following entities have no children. Empty entities.
	TaskEntity(int priority_, bool finished_);
	TaskEntity(int priority_, bool finished_, shared_ptr<Scene> scene);
	//Has child component or components.
	TaskEntity(int priority_, bool finished_, shared_ptr<Scene> scene, shared_ptr <Component> component);
	TaskEntity(int priority_, bool finished_, vector <shared_ptr <Component>> components_list);


	void init();
	void finalize();
	void run();


	~TaskEntity();


};