#pragma once
#include <iostream>
#include <memory>
#include <string>


using namespace std;   

class Entity;

class Component
{
protected:

	shared_ptr<Entity> parent;


public:

	string component_id;
	

	Component(); 
	Component(shared_ptr<Entity> entity);
	Component(shared_ptr<Entity> entity, string component_id);
	~Component();


	virtual void initialize() = 0;
	virtual void update(float deltaTime) = 0;
	virtual string& getId() = 0;
};


