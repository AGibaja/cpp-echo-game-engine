#ifndef TYPES_H
#define TYPES_H

#include <iostream>
#include <glm.hpp>



namespace types
{
	typedef glm::vec3 eVector3;
	typedef glm::vec2 eVector2;
	typedef unsigned  eIndex;
}

#endif // !TYPES_H
