#pragma once
#include <Windows.h>
#include <string>
#include <iostream>
#include <fstream>
#include <rapidxml_utils.hpp>
#include "rapidxml.hpp"
#include "Kernel.hpp"


using namespace rapidxml; 
using namespace std; 

	class FileManager
	{
	public:

		FileManager(string & file_path, shared_ptr<Kernel> kernel);
		~FileManager();

		void printXml(const rapidxml::xml_node<>*node, ...);
		void parseXml(string & file_path);

		//Put XML nodes as string, and then serialize them to entities and other classes.
		//Do it from root first. Provisional, change it later. 
		void serializeXml(const rapidxml::xml_node<>*node);
		void checkNode(const string &node_name);
		
		shared_ptr<Kernel> kernel; 

	private:
		HANDLE handlestd;
		int depth_counter = 0;
		int attribute_depth_counter = 0;
		xml_node<> *rootnode;

		HANDLE getConsoleHandler()
		{
			handlestd = GetStdHandle(STD_OUTPUT_HANDLE);
		}

	};

