#pragma once
#include <vector>
#include <memory>


class Task; 
class Scene; 

using namespace std; 

class Kernel
{

public:
	vector <unique_ptr<Task>> task_list;

	bool running;

	shared_ptr<Scene> scene_kernel; 

	Kernel();

	//Initialize all tasks 
	void init();
	void addTask(unique_ptr<Task> task);
	void runTasks();
	void execute();
	void checkFinalizeTasks();
	void removeLastTask();
	void checkEntities(); 
	void setSceneKernel(shared_ptr<Scene> scene); 
	shared_ptr<Scene> getSceneKernel(); 
};
