#include <iostream>
#include <Dispatcher.hpp>
#include <Entity.hpp>
#include <TransformComponent.hpp>



class ListenerMovement : public Listener
{
	public:

		ListenerMovement(shared_ptr<Entity> entity);

		shared_ptr<Entity> entity_parent; 
		shared_ptr<Component> component_modify; 

		void handle(const Message & message);

};