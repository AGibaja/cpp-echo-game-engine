#pragma once
#include "Component.hpp"
#include <Window.hpp>
#include <RenderModule.hpp>



class ComponentRender : public Component
{

	//Model3D model; Es el modelo a renderizar
	engine::Window *window;

	void update(float delta_time) override;

	void initialize();

	RenderModule::TaskRender *task;
	//NodeRender renderer;
	Scene *scene;
	//Para que compile, quitar mas adelante 
	Entity *entity;

public:

	string component_id; 

	ComponentRender(shared_ptr<Entity>entity);
	ComponentRender(shared_ptr<Entity>entity, string id);


	Task * get_task();

	shared_ptr <Component> createComponent();

	void render();

	void swapBuffersWindow();
};
