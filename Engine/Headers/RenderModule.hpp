#include <Module.hpp>


class RenderModule : public Module
{
public:


	RenderModule::RenderModule(Scene *parent_scene);



	class TaskRender : public Task
	{
		RenderModule &render_module;
		bool finished;

		TaskRender(RenderModule &module);

		void init();
		void run();
		void finalize();
	};
};