#pragma once
#include <iostream>
#include <vector>
#include <Kernel.hpp>
#include <memory>


class Entity;
class Kernel;

using namespace std; 


class Task
{
public: 
	bool finished;
	int priority; 
	string id; 
	shared_ptr <Kernel> kernel_owner; 

public:
	Task();
	Task(int priority_, bool finished_ );

	shared_ptr<Task> getTask();
	shared_ptr<string> getTaskId();
	void setTaskId(string name); 

	//We let other classes define the following pure abstract methods.
	virtual void init() = 0;
	virtual void run() = 0;
	virtual void finalize() = 0;
};


