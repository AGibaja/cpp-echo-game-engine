#include <Task.hpp>



class TaskGroup : Task
{
	std::vector<Task*> task_list;


	//Adds a task to the task list.
	void add_to_task_list(Task &task); 


	//Inits all task list.
	void init_task_list();


	//Finalizes all tasks in task list.
	void finalize_task_list();

};