#include <list>
#pragma once
#include <Listener.hpp>


using namespace std; 


class Dispatcher
{
public: 
	map<string, list<Listener * >> listeners; 

	void addListener(string message_id, Listener * l);

	void send(Message &m);
};