#pragma once
#include <memory>
#include <map>
#include <iostream>
#include <Component.hpp>


class Scene;

class Entity
{

public:

	map <string, shared_ptr<Component>> components;
	shared_ptr <Scene> parent_scene;
	string entity_id; 


	Entity(shared_ptr<Scene> scene);
	Entity(shared_ptr<Scene> scene, string name);
	~Entity();

	shared_ptr<Component> getComponent(string id_component);

	void initialize();
	void update(float deltaTime);
	void addComponent(shared_ptr<Component> &component); 
	void addComponent(const string &name, shared_ptr<Component> &component);
	void printComponents();
	void setEntityId(string name); 

};