#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H


#include "Component.hpp"
#include <Types.hpp>


class TransformComponent : public Component
{
public: 
	glm::vec3 position; 
	string id_component; 
	shared_ptr<Entity> entity_parent; 

	TransformComponent(); 
	TransformComponent(shared_ptr<Entity> entity);
	TransformComponent(string name);
	TransformComponent(shared_ptr<Entity> entity, string name);

	string &getId();
	void translate(glm::vec3 vec);
	void initialize();
	void update(float deltatime);

};

#endif // !TRANSFORMCOMPONENT_H
