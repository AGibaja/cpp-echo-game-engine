#include <Task.hpp>
#include <Dispatcher.hpp>
#include <Scene.hpp>


//Task especialization made to add Listener to entities.
class TaskListener : public Task
{
public: 
	shared_ptr<Entity> entity_owner; 
	shared_ptr<Scene> scene_entity;

	TaskListener();
	TaskListener(int priority_, bool finished_); 
	TaskListener(int priority_, bool finished, shared_ptr<Entity> entity_pointer);
	//We tell the constructor the name of the entity and the scene where the entity is.
	TaskListener(int priority_, bool finished, string entity_id, shared_ptr<Scene> scene); 

	void init(); 
	void run(); 
	void finalize(); 
	void setSceneListener(shared_ptr<Scene> scene); 
};