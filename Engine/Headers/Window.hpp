
// COMENTARIO DE CABECERA

#pragma once

#include <string>
#include <memory>
#include "../../Libraries/SDL2-2.0.7/include/SDL.h"
#include "../../Libraries/glew/include/GL/glew.h"
#include <gl\GL.h>

struct SDL_Window;

namespace engine
{

	class Window
	{

		SDL_Window *sdl_window;
		SDL_GLContext gl_context;

	public:
		const char * wintitle;

		Window(const char * title, int width, int height);
	   ~Window();

	   void makeCurrent(); 
	   void swapBuffers();
	   SDL_Window* getSdlWindow();
	   Window * getWindow();
	};

	

}