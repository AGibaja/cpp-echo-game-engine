#pragma once 
#include <iostream>
#include "Scene.hpp"
#include "Component.hpp"
#include "Task.hpp"
#include "Window.hpp"
#include "Renderer.hpp"


using namespace engine; 


class Module
{

public:
	//Default implementation of Task in case it's null.
	virtual shared_ptr<Component> createComponent();

	shared_ptr <Scene> scene; 

};

