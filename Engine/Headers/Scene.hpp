#pragma once
//Recibir un puntero de la clase Window 
//Kernel por escena. A�adir al crear la escena
//Al cargar el xml se determian que modulos son necesarios dentro de esa escena. Se crean esos modulos y se a�aden al kernel sus tareas si las tienen. 

#include <Window.hpp>
#include <string>
#include <memory>
#include <map>
#include <vector>
#include <Entity.hpp>
#include "Kernel.hpp"
#include "Module.hpp"

class Kernel; 

using namespace std; 
using namespace engine; 


class Scene
{

public:
	shared_ptr<Kernel> kernel; 
	Window * window;
	vector<string> xmldata; 
	map<string, shared_ptr<Entity>> entities;
	string id_scene; 


	Scene(string & file_path, shared_ptr<Kernel> kernel);
	~Scene();
	

	//La escena tiene un mapa de entidades. La key es el nombre de la entidad.
	void execute(); 
	void load(std::string & file_path);
	void addEntity(const string &name, shared_ptr<Entity> &entity);

};

