#include <Component.hpp>
#include <ListenerMovement.hpp>


class ControlComponent : public Component, public Dispatcher
{
public: 
	ControlComponent(shared_ptr <Entity> e);
	ControlComponent(shared_ptr <Entity> e, string id);

	string control_component_id; 

	void initialize(); 
	void update(float delta_time); 
	void handle(const Message & message); 

	
	string &getId();

};