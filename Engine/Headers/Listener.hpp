#ifndef LISTENER_H
#define LISTENER_H


#include <Message.hpp>



class Listener
{
public: 

	Listener(); 

	virtual void handle(const Message & m) = 0; 
};
#endif // !LISTENER_H