// COMENTARIO DE CABECERA

#include <iostream>
#include <Scene.hpp>



using namespace engine;

int _main (int, char **)
{
	Window window("Practica de Motores Gr�ficos y Plugins", 1024, 768);

	/*
		Se llama a la ruta desde la path de Scene.cpp, que es donde se hace la llamada como 
		tal. Desde la linea a continuaci�n de c�digo s�lo se le pasa el argumento. 
	*/

	std::string file_path= "../../Resources/Scene1.xml";
	shared_ptr <Kernel> kernel = make_shared<Kernel>();  
	Scene scene(file_path, kernel);
	scene.execute(); 

	/*
		Aqui como trato en paralelo el tener la ventana abierta y el kernel por detras, llamando a las funciones que necesitan ser llamadas una sola vez?
		Si meto en este exacta l�nea el c�digo que sea, va a seguir hasta el bucle de kernel,
		pero no va a salir nunca de ahi. Como se queda atascado en 
		ese bucle nunca va a llegar al bucle de la ventana. 
		Por ello, habria que encontrar una forma de hacer que los bucles se ejecuten en 
		paralelo.
	*/

	/*
		CONTESTACI�N: crear la ventana y pintar en la ventana lo que hay que pintar es
		responsabilidad del componente renderer. Actualiza la superifice, hace el swap 
		de buffers... 
	*/

	//_______________________________________________________________________________________//

	/*
		Como a�adir las tasks y cuando? Se suponen que para antes de que el kernel empiece 
		ya tiene que haber tasks que pueda ejecutar. Que podria ser una task? Render, por
		ejemplo? Y un mensaje? 
	*/

	/*
		Donde pongo el bucle principal del juego dentro de Engine?
	*/

	//Cuando pasas un shared_ptr se hace por referencia o de alguna forma en especial?

	//Modulos. 
	return 0; 
}
